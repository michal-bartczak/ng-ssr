# ng-ssr

FROM node:7.10.1

RUN npm install -g @angular/cli

WORKDIR /app

CMD ["bash"]
