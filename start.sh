#! /bin/bash

clear;
CONTAINER_NAME="ng-ssr";
running="$(docker inspect -f {{.State.Running}} $CONTAINER_NAME)"

if [ $running = "false" ]; then
  echo "Starting container ng-ssr..."
  docker start ng-ssr;
fi
echo "Exec ng-ssr..."
docker exec -it ng-ssr bash;
