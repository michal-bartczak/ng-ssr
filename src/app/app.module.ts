import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AlertModule } from 'ngx-bootstrap';

import { PlatformService } from './platform.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-ssr'}),
    AlertModule.forRoot()
  ],
  providers: [
    PlatformService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
